Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

<Table("HAO.USERS")>
Partial Public Class USER
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property ID As Long

    <Required>
    <StringLength(40)>
    Public Property NAME As String

    <Required>
    <StringLength(20)>
    Public Property USER_NAME As String

    <Required>
    <StringLength(40)>
    Public Property GMAIL As String

    <StringLength(191)>
    Public Property ADDRESS As String

    Public Property CRETEAD_AT As Date?

    Public Property UPADTED_AT As Date?
End Class
