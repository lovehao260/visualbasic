Imports System
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Partial Public Class UserDBContext
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=UserDBContext")
    End Sub

    Public Overridable Property USERS As DbSet(Of USER)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.Entity(Of USER)() _
            .Property(Function(e) e.NAME) _
            .IsUnicode(False)

        modelBuilder.Entity(Of USER)() _
            .Property(Function(e) e.USER_NAME) _
            .IsUnicode(False)

        modelBuilder.Entity(Of USER)() _
            .Property(Function(e) e.GMAIL) _
            .IsUnicode(False)

        modelBuilder.Entity(Of USER)() _
            .Property(Function(e) e.ADDRESS) _
            .IsUnicode(False)
    End Sub
End Class
