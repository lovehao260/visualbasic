﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.Entity
Imports System.Linq
Imports System.Net
Imports System.Web
Imports System.Web.Mvc
Imports VB1

Namespace Controllers
    Public Class UserController
        Inherits System.Web.Mvc.Controller

        Private db As New UserDBContext

        ' GET: User
        Function Index() As ActionResult
            Return View(db.USERS.ToList())
        End Function

        ' GET: User/Details/5
        Function Details(ByVal id As Long?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim uSER As USER = db.USERS.Find(id)
            If IsNothing(uSER) Then
                Return HttpNotFound()
            End If
            Return View(uSER)
        End Function

        ' GET: User/Create
        Function Create() As ActionResult
            Return View()
        End Function

        ' POST: User/Create
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="NAME,USER_NAME,GMAIL,ADDRESS,CRETEAD_AT,UPADTED_AT")> ByVal uSER As USER) As ActionResult
            If ModelState.IsValid Then
                db.USERS.Add(uSER)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(uSER)
        End Function

        ' GET: User/Edit/5
        Function Edit(ByVal id As Long?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim uSER As USER = db.USERS.Find(id)
            If IsNothing(uSER) Then
                Return HttpNotFound()
            End If
            Return View(uSER)
        End Function

        ' POST: User/Edit/5
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="ID,NAME,USER_NAME,GMAIL,ADDRESS,CRETEAD_AT,UPADTED_AT")> ByVal uSER As USER) As ActionResult
            If ModelState.IsValid Then
                db.Entry(uSER).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(uSER)
        End Function

        ' GET: User/Delete/5
        Function Delete(ByVal id As Long?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim uSER As USER = db.USERS.Find(id)
            If IsNothing(uSER) Then
                Return HttpNotFound()
            End If
            Return View(uSER)
        End Function

        ' POST: User/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Long) As ActionResult
            Dim uSER As USER = db.USERS.Find(id)
            db.USERS.Remove(uSER)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
