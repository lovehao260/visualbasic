﻿@ModelType VB1.USER
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>USER</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.NAME)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.NAME)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.USER_NAME)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.USER_NAME)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.GMAIL)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GMAIL)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ADDRESS)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ADDRESS)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CRETEAD_AT)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CRETEAD_AT)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UPADTED_AT)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UPADTED_AT)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
