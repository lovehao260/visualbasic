﻿@ModelType IEnumerable(Of VB1.USER)
@Code
ViewData("Title") = "Index"
Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.NAME)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.USER_NAME)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.GMAIL)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.ADDRESS)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.CRETEAD_AT)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.UPADTED_AT)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.NAME)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.USER_NAME)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.GMAIL)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.ADDRESS)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.CRETEAD_AT)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.UPADTED_AT)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = item.ID }) |
            @Html.ActionLink("Details", "Details", New With {.id = item.ID }) |
            @Html.ActionLink("Delete", "Delete", New With {.id = item.ID })
        </td>
    </tr>
Next

</table>
