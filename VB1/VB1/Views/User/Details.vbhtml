﻿@ModelType VB1.USER
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Details</h2>

<div>
    <h4>USER</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.NAME)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.NAME)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.USER_NAME)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.USER_NAME)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.GMAIL)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GMAIL)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ADDRESS)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ADDRESS)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CRETEAD_AT)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CRETEAD_AT)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UPADTED_AT)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UPADTED_AT)
        </dd>

    </dl>
</div>
<p>
    @Html.ActionLink("Edit", "Edit", New With { .id = Model.ID }) |
    @Html.ActionLink("Back to List", "Index")
</p>
